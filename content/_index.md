---
title: Ben Moss, PhD
description: building effective feedback loops at all levels of business
type: page
---

# About

I'm a technical leader and consultant that helps businesses create and maintain their IT services more effectively. With over a decade of experience in _true_ DevOps and Continuous Delivery, I've helped highly-regulated organizations achieve some great operational improvements:

{{< achievements >}}
{{< achievement 
	title="Lead times"
	improvement="months/weeks ➡️ hours/mins"
	image="/images/stopwatch.jpg"
	image_author="Tsvetoslav Hristov"
	image_link="https://unsplash.com/photos/QW-f6s9nFIs"
	description="Reducing the the time it takes to deliver changes for software & infrastructure, which also enables more frequent releases"
>}}
{{< achievement 
	title="Recovery times"
	improvement="unknown/days/hours ➡️ known/mins"
	image="/images/recovery.jpg"
	image_author="Piotr Chrobot"
	image_link="https://unsplash.com/photos/M0WbGFRTXqU"
	description="Reducing the time time it takes to recover from component failure and disasterous events, or pre-empt them and take preventative action"
>}}
{{< achievement 
	title="Supportability"
	improvement="escalations ⬇️ 80-100%"
	image="/images/escalators.jpg"
	image_author="Maria Teneva"
	image_link="https://unsplash.com/photos/7Sgw_56YVQI"
	description="Reductions in 2nd- and 3rd-line support escalations where support teams previously had to wait on specialist knowledge in order to keep services running"
>}}
{{< /achievements >}}

## Recently...

In 2021, I founded **Kaizen Path Consulting Ltd** ([KPC](https://kaizenpath.co.uk/)) - a freelance Tech consultancy specializing in modern engineering practices (infra-as-code, continuous delivery, kanban, etc.). Through KPC, I have **delivered a number of projects for clients in the UK and US**, ranging from **large-scale organizational transformations** for multinationals to **bespoke software/tooling** and **training/coaching** for smaller clients:

- **Target operating model (TOM) for CIO function of a multinational** - simplifying and standardizing a complex heterogeneous landscape of c.1000 staff running 100s digital products in multiple countries/time zones. Co-led project working directly for the newly-appointed CIO via [Axiologik](https://axiologik.com/) (Sep-Oct 2021).
- **Tech Service Improvement Programme Leadership for CIO function of a multinational** - enabling organizational transition from holding to operating company. Provided senior leadership for 3 project teams over a 14-month period (via [Axiologik](https://axiologik.com/)) - from initial discovery through delivery and transition to BAU. Delivered and embedded modern engineering capability, practice, and thinking into the service operations teams across the globe, leveraging my broad industry and enterprise engineering experience. Projects included elements of BI dashboarding, DORA metrics, engineering quality standards, service management, and governance/compliance. Engagement grew to around 50 consultants; and I remained one of the key senior leaders throughout its duration (Nov 2021-Dec 2022).
- **Service design for a management consultancy** - broadening the range of service offerings for their clients. The offerings relate to areas such as target operating models, value stream management, and modern engineering practices (Q1-Q2 2023).
- **Operational strategies for digital media clients** - allowing clients to effectively manage ever-expanding collections of diverse digital assets with minimal OpEx. Design and delivery of digital asset management systems (software and business processes) allowing clients to archive, store and use image and audio data safely and more effectively. Included both strategy for consolidating extant data and software to govern compliance to new standards as part of BAU processes (Q2-Q4 2023).

Prior to KPC, I built and led the teams that deliver and support a number of automation platforms that leverage infra-as-code:

- **Centiq's automation platform for SAP** - automating deployments and operations across heterogeneous customer landscapes, enabling the business to scale its services (2017-2021)
- **Thames Water's SAP automation platform** - building and managing clustered SAP HANA landscapes in Azure with regional disaster recovery (2018-2019)
- **Capital One’s self-service CI/CD platform** - safer software builds and releases throughout UK tech teams (Mobile, Web, API, Infrastructure, and Data), and even for a few teams in the US too (2014-2016)

Throughout 2020, I also led the team that collaborated with Microsoft Azure Engineering in the US on [their open source initiative](https://github.com/Azure/sap-hana); delivering [HA HANA Clustering](https://github.com/Azure/sap-hana/projects/3), along with the [SAP Application Tier](https://github.com/Azure/sap-hana/projects/4), and [SAP Software Install Framework](https://github.com/Azure/sap-hana/projects/10).

## Previously...

I spent a number of years developing software (mainly Java APIs) for centralized admin of trading platforms at [Thomson Reuters](https://www.thomsonreuters.com) (now part of [LSEG](https://www.lseg.com)). Alongside leading on a few projects, I got to spent a good deal of time working closely with remote Ops teams in London and Bangkok (2007-2014).

Prior to that, I was an academic researcher/lecturer in Crypto/Security and Computer-aided Learning at the [University of Nottingham](https://www.nottingham.ac.uk) alonside completing my PhD in Crypto/Security with funding from [British Telecom Research Labs](https://www.bt.com/about/innovation/bt-labs) (2001-2007).

## Short Bio

Ben Moss is a commercially-aware, hands-on tech leader with over 15 years’ experience in FinTech, professional, and managed services. He has a background in software engineering and passion for systems thinking, so his career has paralleled the DevOps movement. He's spent the last decade in leadership and consulting roles helping highly-regulated companies improve their value streams and respond to a wide range of tech and business challenges. His latest interests are CI/CD automated compliance and the socio-technical factors that assist/inhibit organizational learning.
