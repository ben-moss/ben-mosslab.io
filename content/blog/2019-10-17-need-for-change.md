---
title: Infrastructure as Code
description: the relentless need for business change
linkedin_url: https://www.linkedin.com/pulse/relentless-need-business-change-infrastructure-code-ben-moss/
external_url: https://www.centiq.co.uk/blog/need-for-change-iac/
date: 2019-10-17T09:00:00+01:00
featured_image: 'images/blog/need-for-change.jpg'
category: blog
tags: ['centiq', 'devops', 'iac', 'sap', 'sap landscape management']
---

> Note: This article originally appeared [on the Centiq Blog](https://www.centiq.co.uk/blog/need-for-change-iac/) and [on my LinkedIn](https://www.linkedin.com/pulse/relentless-need-business-change-infrastructure-code-ben-moss/) the following day.

![Change vs Stability](/images/blog/need-for-change.jpg)

Infrastructure as Code (IaC) is a term used to describe a collection of practices for managing IT resources via programmable interfaces, allowing infrastructure to be defined in code, rather than created through a series of human activity. Typically, infrastructure resources might include various types of network, storage, and compute components, alongside resources relating to identity and permissions.

<!--more-->

The need for IaC arose out of the growing number of problems encountered trying to manage increasingly complex IT using traditional methods of operation. The demands of modern business typically bring the need to scale – both in volume and frequency, whilst maintaining quality and consistency. This typically results in compromising stability to meet a relentless need for business change.

Defining infrastructure as code goes way beyond scripting or config management – it enables engineers to leverage many of the well-established good practices that software engineers have been using for decades. Not just fundamental programming constructs like variables, conditionals and loops, but higher level abstractions such as design patterns, and highly effective practices such as version control, dependency management, automated testing, continuous integration, and continuous delivery.

Centiq leverage IaC to remove the complexity of SAP landscape management for our customers. We transform complex operational activities into simple automated workflows such as _build system_, _test system_, _stop/start system_, or _failover system_ and we keep this consistent across all system types whether or not they are clustered, or have DR capabilities, and also across different SAP products such as S/4HANA or BW/4HANA. Even OS-specific differences are encapsulated within the workflows, so the user experience is identical no matter what OS you’re running.

In future posts, I’ll explore some of the ways we incorporate IaC practices into our projects and services at Centiq, and the value it's added for our customers.
