---
title: Make Work Visible
event: Capital One Q3 All Hands 2015
location: Nottingham
date: 2015-07-14T08:00:00+01:00
featured_image: 'images/talks/c1-make-work-visible.jpg'
category: talk
tags: ['capital one', 'all hands', 'agile', 'principles', 'kanban']
---

I gave this [All Hands](https://dictionary.cambridge.org/dictionary/english/all-hands) talk to the Tech organization at Capital One (~150 attendees) on _Making Work Visible_ - one of several agile principles we established as part of the Agile Community of Practice.
