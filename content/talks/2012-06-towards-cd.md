---
title: Towards Continuous Delivery in Transactions Admin
event: Thomson Reuters Tech Unconference 2012
location: Shoreditch, London
date: 2012-06-15T08:00:00+01:00
featured_image: 'images/talks/tr-towards-cd.jpg'
category: talk
tags: ['thomson reuters', 'unconference', 'ci/cd', 'continuous delivery']
---

Presenting the progress my team had been making from long-time CI practitioners towards many of the practices required to achieve continuous delivery - keeping our codebase shippable.

<!--more-->

I talked about some of the many challenges we'd encountered from the technical, business, social and personal:

- Growing the team
- Integration
- Delivery bottlenecks
- Late collaboration
- Regression deficit

Then described how we had overcome some of these with CD principles/practices and the benefits it provides:

- Visibility int the status of our customer value
- Traceability on the impact of a change
- Stability of the system
- Compliance with repeatability and justification of changes
- Faster feedback on changes as much of the information becomes on-demand

The session was concluded with a view of our CD roadmap for the rest of the year, and a demo of a mockup pipeline in the latest [GoCD](https://www.gocd.org/) release.
