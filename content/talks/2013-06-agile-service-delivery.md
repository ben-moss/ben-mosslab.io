---
title: Evolution Towards Agile Service Delivery
event: Thomson Reuters Tech Unconference 2013
location: Shoreditch, London
date: 2013-06-14T08:00:00+01:00
featured_image: 'images/talks/tr-agile-service-delivery.jpg'
category: talk
tags: ['thomson reuters', 'unconference', 'agile', 'service delivery', 'devops']
---

Presenting some of my recent work on "Agile ways of working in service delivery" (essentially I was talking about DevOps). In this talk I described some of the software engineering practices I'd been using to improve the life of our Operations teams.

<!--more-->

Touching on aspects of the system architecture of an automation engine I'd built to handle some common Operational processes, this included:

- Log gathering
- System stop/starts that handled Veritas cluster manager
- App and DB deployments/upgrades
- DB backups/restores
- App and DB Cluster failover
- App and DB Site failover

Essentially this was a kind of "poor man's Ansible", before I knew ansible existed. It was built in perl and needed to be distributed to each node under management - early days in my own personal IaC journey. Using the [go script principles](https://www.thoughtworks.com/insights/blog/praise-go-script-part-i) it created a common language between Development and Operations (and other teams along the way). As our product was called _TOPS_, I named this the _TOPS Ops Console_.
