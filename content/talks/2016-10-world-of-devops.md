---
title: The World of DevOps (and how we all play a part)
event: Centiq Interview
location: Nottingham
date: 2016-10-14T08:00:00+01:00
featured_image: 'images/talks/world-of-devops.jpg'
category: talk
tags: ['centiq', 'interview', 'devops', 'coaching']
---

Presentation delivered as part of my Centiq interview that described my background, experience, and several examples of where I'd used my experience of DevOps thinking to add business value in previous organizations.

<!--more-->

The purpose was to understand some of the existing challenges within the organization, and identify where I might add value whilst averting assumptions that "the DevOps person is someone to hand off our problems to".
