---
title: Mobile iOS Build Pipeline
event: Capital One Q4 Release Train Demo 2014
location: Nottingham
date: 2014-12-02T08:00:00+01:00
featured_image: 'images/talks/c1-mobile-pipeline.jpg'
category: talk
tags: ['capital one', 'release train', 'safe', 'agile', 'mobile', 'build pipeline', 'delivery pipeline']
---

My team's first major demo on our automation of the entire mobile build/delivery pipeline for the Capital One UK iOS Mobile App. This was presented the CEO, CTO and other senior stakeholders in the business, and was accomanied by a live demo of pushing a change through the entire process onto a real smartphone in just a few minutes - running through automated QA and security testing, and deployed to a test instance of the App Store using [Hockey App](https://hockeyapp.net) (now rebranded App Center).
